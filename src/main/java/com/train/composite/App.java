package com.train.composite;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author tetiana on 25.10.18
 */
public class App {
    public static void main(String[] args) {

        IFile[] arr = {new File("file1"), new File("file2")};
        Collection<IFile> files = Arrays.asList(arr);
        Folder folder = new Folder(files);
        folder.getName();
    }
}
