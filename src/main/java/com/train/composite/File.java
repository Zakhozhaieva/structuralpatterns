package com.train.composite;

/**
 * @author tetiana on 25.10.18
 */
public class File implements IFile {

    private String name;

    public File(String name){
        this.name = name;
    }

    @Override
    public void getName() {
        System.out.println("name = " + name);
    }
}
