package com.train.composite;

import java.util.Collection;
import java.util.LinkedList;

/**
 * @author tetiana on 25.10.18
 */
public class Folder implements IFile {

    private Collection<IFile> files = new LinkedList<>();

    public Folder(Collection<IFile> files){
        this.files = files;
    }

    @Override
    public void getName() {
        // TODO STUB
        for(IFile file: files){
            file.getName();
        }
    }
}
