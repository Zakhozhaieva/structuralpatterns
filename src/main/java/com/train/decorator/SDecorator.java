package com.train.decorator;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author tetiana on 24.10.18
 */
public class SDecorator {
    private List<Character> innerCharacters = new LinkedList<>();

    public SDecorator() {
        innerCharacters = new LinkedList<>();
    }

    public SDecorator(String innerString) {
        char[] chars = innerString.toCharArray();
        for (char c : chars) {
            innerCharacters.add(c);
        }
    }

    private List<Character> convertToChars(String str) {
        char[] chars = str.toCharArray();
        List<Character> characters = new LinkedList<>();
        for(char c: chars){
            characters.add(c);
        }
        return characters;
    }

    public SDecorator append(String str) {
        List<Character> characters = new LinkedList<>();
        if (str != null && !str.isEmpty()) {
            characters = convertToChars(str);
        } else if (str == null) {
            characters = convertToChars("null");
        }
        innerCharacters.addAll(characters);
        return this;
    }

    public SDecorator append(int i) {
        String s = String.valueOf(i);
        List<Character> characters = convertToChars(s);
        innerCharacters.addAll(characters);
        return this;
    }

    public SDecorator append(long l) {
        String s = String.valueOf(l);
        List<Character> characters = convertToChars(s);
        innerCharacters.addAll(characters);
        return this;
    }

    public SDecorator append(float f) {
        String s = String.valueOf(f);
        List<Character> characters = convertToChars(s);
        innerCharacters.addAll(characters);
        return this;
    }

    public SDecorator append(double d) {
        String s = String.valueOf(d);
        List<Character> characters = convertToChars(s);
        innerCharacters.addAll(characters);
        return this;
    }

    public SDecorator append(boolean b) {
        String s = String.valueOf(b);
        List<Character> characters = convertToChars(s);
        innerCharacters.addAll(characters);
        return this;
    }

    public void setLength(int len){
        innerCharacters = innerCharacters.subList(0, len);
    }

    @Override
    public String toString() {
        return innerCharacters.stream().map(e -> e.toString()).collect(Collectors.joining());
    }
}
