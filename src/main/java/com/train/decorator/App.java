package com.train.decorator;


/**
 * @author tetiana on 23.10.18
 */
public class App {

    public static void main(String[] args) {
        testDecorator();
    }

    private static void testDecorator() {
        SDecorator decor = new SDecorator("asdd");
        decor.append(null);
        decor.append(1);
        decor.append(2);
        decor.append("contentt");
        decor.append(true);
        decor.append(false);

        System.out.println(" testDecorator >> " + decor);

        decor.setLength(4);

        System.out.println("testDecor after setLength >> " + decor);

        decor.setLength(0);
        System.out.println("testDecor after 0 length >>> " + decor);
    }
}
